import 'package:stayscanner/dto/Response.dart';

class Booking extends Response{
  int amtOfResults;
  int status;
  List<Results> results;
  String provider;
  bool isComplete;

  Booking(
      {this.amtOfResults,
        this.status,
        this.results,
        this.provider,
        this.isComplete});

  Booking.fromJson(Map<String, dynamic> json) {
    amtOfResults = json['amtOfResults'];
    status = json['status'];
    if (json['results'] != null) {
      results = new List<Results>();
      json['results'].forEach((v) {
        results.add(new Results.fromJson(v));
      });
    }
    provider = json['provider'];
    isComplete = json['isComplete'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['amtOfResults'] = this.amtOfResults;
    data['status'] = this.status;
    if (this.results != null) {
      data['results'] = this.results.map((v) => v.toJson()).toList();
    }
    data['provider'] = this.provider;
    data['isComplete'] = this.isComplete;
    return data;
  }
}

class Results {
  Data data;
  String provider;
  String hid;
  bool isInstantBook;
  List<double> latLng;
  Prices prices;
  String url;
  int zScore;
  Ids ids;

  Results(
      {this.data,
        this.provider,
        this.hid,
        this.isInstantBook,
        this.latLng,
        this.prices,
        this.url,
        this.zScore,
        this.ids});

  Results.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    provider = json['provider'];
    hid = json['hid'];
    isInstantBook = json['isInstantBook'];
    latLng = json['latLng'].cast<double>();
    prices =
    json['prices'] != null ? new Prices.fromJson(json['prices']) : null;
    url = json['url'];
    zScore = json['zScore'];
    ids = json['ids'] != null ? new Ids.fromJson(json['ids']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['provider'] = this.provider;
    data['hid'] = this.hid;
    data['isInstantBook'] = this.isInstantBook;
    data['latLng'] = this.latLng;
    if (this.prices != null) {
      data['prices'] = this.prices.toJson();
    }
    data['url'] = this.url;
    data['zScore'] = this.zScore;
    if (this.ids != null) {
      data['ids'] = this.ids.toJson();
    }
    return data;
  }
}

class Data {
  String address;
  int availableRooms;
//  double distanceFromVenue;
  HotelProvider hotelProvider;
  String logo;
  String name;
  String rawid;
  int rating;
  String roomInfo;
  int stars;
  String thumb;
  String type;
  String urlDirect;
  List<Rates> rates;
  bool isDeal;

  Data(
      {this.address,
        this.availableRooms,
//        this.distanceFromVenue,
        this.hotelProvider,
        this.logo,
        this.name,
        this.rawid,
        this.rating,
        this.roomInfo,
        this.stars,
        this.thumb,
        this.type,
        this.urlDirect,
        this.rates,
        this.isDeal});

  Data.fromJson(Map<String, dynamic> json) {
    address = json['address'];
    availableRooms = json['availableRooms'];
//    distanceFromVenue = json['distanceFromVenue'];
    hotelProvider = json['hotelProvider'] != null
        ? new HotelProvider.fromJson(json['hotelProvider'])
        : null;
    logo = json['logo'];
    name = json['name'];
    rawid = json['rawid'];
    rating = json['rating'];
    roomInfo = json['roomInfo'];
    stars = json['stars'] != null ? json['stars'] : 0;
    thumb = json['thumb'];
    type = json['type'];
    urlDirect = json['urlDirect'];
    if (json['rates'] != null) {
      rates = new List<Rates>();
      json['rates'].forEach((v) {
        rates.add(new Rates.fromJson(v));
      });
    }
    isDeal = json['isDeal'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['address'] = this.address;
    data['availableRooms'] = this.availableRooms;
//    data['distanceFromVenue'] = this.distanceFromVenue;
    if (this.hotelProvider != null) {
      data['hotelProvider'] = this.hotelProvider.toJson();
    }
    data['logo'] = this.logo;
    data['name'] = this.name;
    data['rawid'] = this.rawid;
    data['rating'] = this.rating;
    data['roomInfo'] = this.roomInfo;
    data['stars'] = this.stars;
    data['thumb'] = this.thumb;
    data['type'] = this.type;
    data['urlDirect'] = this.urlDirect;
    if (this.rates != null) {
      data['rates'] = this.rates.map((v) => v.toJson()).toList();
    }
    data['isDeal'] = this.isDeal;
    return data;
  }
}

class HotelProvider {
  String code;
  String name;
  String logo;

  HotelProvider({this.code, this.name, this.logo});

  HotelProvider.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    name = json['name'];
    logo = json['logo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['name'] = this.name;
    data['logo'] = this.logo;
    return data;
  }
}

class Rates {
  int total;
  String totalF;
  String nightlyF;
  double nightly;
  HotelProvider hotelProvider;
  String url;

  Rates(
      {this.total,
        this.totalF,
        this.nightlyF,
        this.nightly,
        this.hotelProvider,
        this.url});

  Rates.fromJson(Map<String, dynamic> json) {
    total = json['total'];
    totalF = json['total_f'];
    nightlyF = json['nightly_f'];
    nightly = json['nightly'].toDouble();
    hotelProvider = json['hotelProvider'] != null
        ? new HotelProvider.fromJson(json['hotelProvider'])
        : null;
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    data['total_f'] = this.totalF;
    data['nightly_f'] = this.nightlyF;
    data['nightly'] = this.nightly;
    if (this.hotelProvider != null) {
      data['hotelProvider'] = this.hotelProvider.toJson();
    }
    data['url'] = this.url;
    return data;
  }
}

class Prices {
  int total;
  String totalF;
  String nightlyF;
  double nightly;

  Prices({this.total, this.totalF, this.nightlyF, this.nightly});

  Prices.fromJson(Map<String, dynamic> json) {
    total = json['total'];
    totalF = json['total_f'];
    nightlyF = json['nightly_f'];
    nightly = json['nightly'].toDouble();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    data['total_f'] = this.totalF;
    data['nightly_f'] = this.nightlyF;
    data['nightly'] = this.nightly;
    return data;
  }
}

class Ids {
  String bkID;
  String hcID;
  String overwriteID;

  Ids({this.bkID, this.hcID, this.overwriteID});

  Ids.fromJson(Map<String, dynamic> json) {
    bkID = json['bkID'];
    hcID = json['hcID'];
    overwriteID = json['overwriteID'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bkID'] = this.bkID;
    data['hcID'] = this.hcID;
    data['overwriteID'] = this.overwriteID;
    return data;
  }
}
