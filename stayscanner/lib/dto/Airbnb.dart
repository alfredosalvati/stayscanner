import 'package:stayscanner/dto/Response.dart';

class Airbnb extends Response {
  int amtOfResults;
  bool isComplete;
  List<Results> results;
  String provider;
  String category;
  int status;

  Airbnb(
      {this.amtOfResults,
        this.isComplete,
        this.results,
        this.provider,
        this.category,
        this.status});

  Airbnb.fromJson(Map<String, dynamic> json) {
    amtOfResults = json['amtOfResults'];
    isComplete = json['isComplete'];
    if (json['results'] != null) {
      results = new List<Results>();
      json['results'].forEach((v) {
        results.add(new Results.fromJson(v));
      });
    }
    provider = json['provider'];
    category = json['category'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['amtOfResults'] = this.amtOfResults;
    data['isComplete'] = this.isComplete;
    if (this.results != null) {
      data['results'] = this.results.map((v) => v.toJson()).toList();
    }
    data['provider'] = this.provider;
    data['category'] = this.category;
    data['status'] = this.status;
    return data;
  }
}

class Results {
  String hid;
  bool isInstantBook;
  Data data;
  List<double> latLng;
  Rates prices;
  String url;
  int zScore;

  Results(
      {this.hid,
        this.isInstantBook,
        this.data,
        this.latLng,
        this.prices,
        this.url,
        this.zScore});

  Results.fromJson(Map<String, dynamic> json) {
    hid = json['hid'];
    isInstantBook = json['isInstantBook'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    latLng = json['latLng'].cast<double>();
    prices = json['prices'] != null ? new Rates.fromJson(json['prices']) : null;
    url = json['url'];
    zScore = json['zScore'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['hid'] = this.hid;
    data['isInstantBook'] = this.isInstantBook;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['latLng'] = this.latLng;
    if (this.prices != null) {
      data['prices'] = this.prices.toJson();
    }
    data['url'] = this.url;
    data['zScore'] = this.zScore;
    return data;
  }
}

class Data {
  String name;
  String address;
  String thumb;
  bool isAirbnbPlus;
  bool isAirbnbLuxe;
  bool isSuperHost;
  HotelProvider hotelProvider;
  String rating;
  double stars;
  int reviewCount;
  String type;
  int beds;
  int guests;
  String urlDirect;
  List<Rates> rates;
  bool isAirbnb;

  Data(
      {this.name,
        this.address,
        this.thumb,
        this.isAirbnbPlus,
        this.isAirbnbLuxe,
        this.isSuperHost,
        this.hotelProvider,
        this.rating,
        this.reviewCount,
        this.type,
        this.beds,
        this.guests,
        this.urlDirect,
        this.rates,
        this.isAirbnb});

  Data.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    address = json['address'];
    thumb = json['thumb'];
    isAirbnbPlus = json['isAirbnbPlus'];
    isAirbnbLuxe = json['isAirbnbLuxe'];
    isSuperHost = json['isSuperHost'];
    hotelProvider = json['hotelProvider'] != null
        ? new HotelProvider.fromJson(json['hotelProvider'])
        : null;
    rating = json['rating'].toString();
    stars = json['stars'] != null ? json['stars'].toDouble() : 0.0;
    reviewCount = json['reviewCount'];
    type = json['type'];
    beds = json['beds'];
    guests = json['guests'];
    urlDirect = json['urlDirect'];
    if (json['rates'] != null) {
      rates = new List<Rates>();
      json['rates'].forEach((v) {
        rates.add(new Rates.fromJson(v));
      });
    }
    isAirbnb = json['isAirbnb'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['address'] = this.address;
    data['thumb'] = this.thumb;
    data['isAirbnbPlus'] = this.isAirbnbPlus;
    data['isAirbnbLuxe'] = this.isAirbnbLuxe;
    data['isSuperHost'] = this.isSuperHost;
    if (this.hotelProvider != null) {
      data['hotelProvider'] = this.hotelProvider.toJson();
    }
    data['rating'] = this.rating;
    data['reviewCount'] = this.reviewCount;
    data['type'] = this.type;
    data['beds'] = this.beds;
    data['guests'] = this.guests;
    data['urlDirect'] = this.urlDirect;
    if (this.rates != null) {
      data['rates'] = this.rates.map((v) => v.toJson()).toList();
    }
    data['isAirbnb'] = this.isAirbnb;
    return data;
  }
}

class HotelProvider {
  String code;
  String name;
  String logo;

  HotelProvider({this.code, this.name, this.logo});

  HotelProvider.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    name = json['name'];
    logo = json['logo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['name'] = this.name;
    data['logo'] = this.logo;
    return data;
  }
}

class Rates {
  double nightly;
  int total;
  String nightlyF;
  String totalF;
  HotelProvider hotelProvider;
  String url;

  Rates(
      {this.nightly,
        this.total,
        this.nightlyF,
        this.totalF,
        this.hotelProvider,
        this.url});

  Rates.fromJson(Map<String, dynamic> json) {
    nightly = double.parse(json['nightly'].toString());
    total = json['total'];
    nightlyF = json['nightly_f'];
    totalF = json['total_f'];
    hotelProvider = json['hotelProvider'] != null
        ? new HotelProvider.fromJson(json['hotelProvider'])
        : null;
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nightly'] = this.nightly;
    data['total'] = this.total;
    data['nightly_f'] = this.nightlyF;
    data['total_f'] = this.totalF;
    if (this.hotelProvider != null) {
      data['hotelProvider'] = this.hotelProvider.toJson();
    }
    data['url'] = this.url;
    return data;
  }
}
