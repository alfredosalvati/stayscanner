import 'package:stayscanner/dto/Response.dart';

class Hotel extends Response{
  int amtOfResults;
  bool isComplete;
  List<Results> results;
  String provider;
  int status;

  Hotel(
      {this.amtOfResults,
        this.isComplete,
        this.results,
        this.provider,
        this.status});

  Hotel.fromJson(Map<String, dynamic> json) {
    amtOfResults = json['amtOfResults'];
    isComplete = json['isComplete'];
    if (json['results'] != null) {
      results = new List<Results>();
      json['results'].forEach((v) {
        results.add(new Results.fromJson(v));
      });
    }
    provider = json['provider'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['amtOfResults'] = this.amtOfResults;
    data['isComplete'] = this.isComplete;
    if (this.results != null) {
      data['results'] = this.results.map((v) => v.toJson()).toList();
    }
    data['provider'] = this.provider;
    data['status'] = this.status;
    return data;
  }
}

class Results {
  String hid;
  String fsid;
  Data data;
  bool isInstantBook;
  List<double> latLng;
  Prices prices;
  String url;
  int zScore;

  Results(
      {this.hid,
        this.fsid,
        this.data,
        this.isInstantBook,
        this.latLng,
        this.prices,
        this.url,
        this.zScore});

  Results.fromJson(Map<String, dynamic> json) {
    hid = json['hid'];
    fsid = json['fsid'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    isInstantBook = json['isInstantBook'];
    latLng = json['latLng'].cast<double>();
    prices =
    json['prices'] != null ? new Prices.fromJson(json['prices']) : null;
    url = json['url'];
    zScore = json['zScore'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['hid'] = this.hid;
    data['fsid'] = this.fsid;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['isInstantBook'] = this.isInstantBook;
    data['latLng'] = this.latLng;
    if (this.prices != null) {
      data['prices'] = this.prices.toJson();
    }
    data['url'] = this.url;
    data['zScore'] = this.zScore;
    return data;
  }
}

class Data {
  String address;
  String beds;
//  double distanceFromVenue;
  String guests;
  HotelProvider hotelProvider;
  String hcurl;
  String logo;
  String name;
  int rawid;
  String rating;
  List<Rates> rates;
  String roomInfo;
  int reviewCount;
  int stars;
  String tittiesKey;
  String thumb;
  String type;
  bool isHC;

  Data(
      {this.address,
        this.beds,
//        this.distanceFromVenue,
        this.guests,
        this.hotelProvider,
        this.hcurl,
        this.logo,
        this.name,
        this.rawid,
        this.rating,
        this.rates,
        this.roomInfo,
        this.reviewCount,
        this.stars,
        this.tittiesKey,
        this.thumb,
        this.type,
        this.isHC});

  Data.fromJson(Map<String, dynamic> json) {
    address = json['address'];
    beds = json['beds'];
//    distanceFromVenue = json['distanceFromVenue'];
    guests = json['guests'];
    hotelProvider = json['hotelProvider'] != null
        ? new HotelProvider.fromJson(json['hotelProvider'])
        : null;
    hcurl = json['hcurl'];
    logo = json['logo'];
    name = json['name'];
    rawid = json['rawid'];
    rating = json['rating'];
    if (json['rates'] != null) {
      rates = new List<Rates>();
      json['rates'].forEach((v) {
        rates.add(new Rates.fromJson(v));
      });
    }
    roomInfo = json['roomInfo'];
    reviewCount = json['reviewCount'];
    stars = json['stars'];
    tittiesKey = json['tittiesKey'];
    thumb = json['thumb'];
    type = json['type'];
    isHC = json['isHC'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['address'] = this.address;
    data['beds'] = this.beds;
//    data['distanceFromVenue'] = this.distanceFromVenue;
    data['guests'] = this.guests;
    if (this.hotelProvider != null) {
      data['hotelProvider'] = this.hotelProvider.toJson();
    }
    data['hcurl'] = this.hcurl;
    data['logo'] = this.logo;
    data['name'] = this.name;
    data['rawid'] = this.rawid;
    data['rating'] = this.rating;
    if (this.rates != null) {
      data['rates'] = this.rates.map((v) => v.toJson()).toList();
    }
    data['roomInfo'] = this.roomInfo;
    data['reviewCount'] = this.reviewCount;
    data['stars'] = this.stars;
    data['tittiesKey'] = this.tittiesKey;
    data['thumb'] = this.thumb;
    data['type'] = this.type;
    data['isHC'] = this.isHC;
    return data;
  }
}

class HotelProvider {
  String code;
  String name;
  String logo;

  HotelProvider({this.code, this.name, this.logo});

  HotelProvider.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    name = json['name'];
    logo = json['logo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['name'] = this.name;
    data['logo'] = this.logo;
    return data;
  }
}

class Rates {
  int nightly;
  String nightlyF;
  int total;
  String totalF;
  RoomInfo roomInfo;
  HotelProvider hotelProvider;
  String url;

  Rates(
      {this.nightly,
        this.nightlyF,
        this.total,
        this.totalF,
        this.roomInfo,
        this.hotelProvider,
        this.url});

  Rates.fromJson(Map<String, dynamic> json) {
    nightly = json['nightly'];
    nightlyF = json['nightly_f'];
    total = json['total'];
    totalF = json['total_f'];
    roomInfo = json['roomInfo'] != null
        ? new RoomInfo.fromJson(json['roomInfo'])
        : null;
    hotelProvider = json['hotelProvider'] != null
        ? new HotelProvider.fromJson(json['hotelProvider'])
        : null;
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nightly'] = this.nightly;
    data['nightly_f'] = this.nightlyF;
    data['total'] = this.total;
    data['total_f'] = this.totalF;
    if (this.roomInfo != null) {
      data['roomInfo'] = this.roomInfo.toJson();
    }
    if (this.hotelProvider != null) {
      data['hotelProvider'] = this.hotelProvider.toJson();
    }
    data['url'] = this.url;
    return data;
  }
}

class RoomInfo {
  String roomName;
  int numberOfRooms;

  RoomInfo({this.roomName, this.numberOfRooms});

  RoomInfo.fromJson(Map<String, dynamic> json) {
    roomName = json['roomName'];
    numberOfRooms = json['numberOfRooms'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['roomName'] = this.roomName;
    data['numberOfRooms'] = this.numberOfRooms;
    return data;
  }
}

class Prices {
  int nightly;
  int total;
  String nightlyF;
  String totalF;

  Prices({this.nightly, this.total, this.nightlyF, this.totalF});

  Prices.fromJson(Map<String, dynamic> json) {
    nightly = json['nightly'];
    total = json['total'];
    nightlyF = json['nightly_f'];
    totalF = json['total_f'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nightly'] = this.nightly;
    data['total'] = this.total;
    data['nightly_f'] = this.nightlyF;
    data['total_f'] = this.totalF;
    return data;
  }
}
