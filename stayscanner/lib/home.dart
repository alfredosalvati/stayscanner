import 'dart:async';
import 'dart:convert';
import 'dart:ui' as ui;

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/intl.dart';
import 'package:currency_pickers/country.dart';
import 'package:currency_pickers/utils/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stayscanner/common/Search.dart';
import 'package:stayscanner/currency/currency_picker_dropdown.dart' ;
import 'package:date_format/date_format.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart' hide RangeSlider ;
import 'package:flutter/rendering.dart';
import 'package:flutter_widgets/flutter_widgets.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart' ;
import 'package:google_maps_webservice/places.dart' hide Location;
import 'package:location/location.dart';
import 'package:shimmer/shimmer.dart';
import 'package:stayscanner/common/colors.dart';
import 'package:stayscanner/custom_slider/flutter_range_slider.dart';
import 'package:stayscanner/custom_icons//my_flutter_app_icons.dart' as CustomIcon;
import 'package:stayscanner/util/dialogs.dart';
import 'package:stayscanner/util/http_util.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vibration/vibration.dart';
import 'package:stayscanner/common/constants.dart';
import 'package:connectivity/connectivity.dart';

import 'package:smooth_star_rating/smooth_star_rating.dart';



import 'common/constants.dart';
import 'date/data_picker.dart';
import 'domain/when.dart';
import 'maps/google_places.dart';
import 'util/stay.dart';


class Home extends StatefulWidget  {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin{

  SharedPreferences prefs;

  PageController pageController;

  var currentPageValue = 0.0;

  AnimationController animationController;
  Animation animation;

  var focusPlaceNode;

  var mapVisible = true;

  var addGuest = false;

  int adults = 1;
  int infants = 0;
  int children = 0;

  int rooms = 1;

  //chosen location?
  var choose = false;
  bool canVibrate = false;

  var homeSelect = true;


  LocationData currentLocation;

  BoxShadow boxShadowOne = BoxShadow(
    color: Colors.transparent,

  );

  BoxShadow boxShadowZero = BoxShadow(
    color: Colors.white,
    offset: new Offset(1.0, 1.0),
    blurRadius: 10.0,
  );

  HttpUtil http;
  StreamSubscription<ConnectivityResult> subscription;


  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance().then( (value) => prefs = value);

    subscription = Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      if (result == ConnectivityResult.mobile || result == ConnectivityResult.wifi) {
        print("Connected to Internet");
        Navigator.of(_keyLoader.currentContext,rootNavigator: true).pop();//close the dialoge

      } else {
        Dialogs.showLoadingDialog(context, _keyLoader, true);//invoking login
      }
    });

    http = new HttpUtil();

    getUserLocation();
    isVibrate();

    pageController = PageController();
    focusPlaceNode = new FocusNode();

    animationController = AnimationController(vsync: this, duration: Duration(seconds: 10));
    animation = Tween(begin: 0.7, end: 0.0).animate(CurvedAnimation(parent: animationController, curve: Curves.elasticOut));

    pageController.addListener(() {
      setState(() {
        currentPageValue = pageController.page;
      });
    });


    focusPlaceNode.addListener(() {
      if (focusPlaceNode.hasFocus) {
        setState(() {
          city = "";
          mapVisible = true;
        });

      }
    });

    itemPositionListener.itemPositions.addListener( () {

      for (var element in itemPositionListener.itemPositions.value) {
//        print(element.index);
//        print(element.itemLeadingEdge);
//        print(element.itemTrailingEdge);

        int index = element.index;

        if (element.itemLeadingEdge > 0.02 && element.itemTrailingEdge < 0.90 && selectedPage != index) {
          // ANIMATE MARKERS

          futureStays.then((onValue) {
            Stay stay = http.fetchedStays.asMap()[index];
            stay.isSelected = true;

            Marker markerToRemove = _markers.where((o) =>
            o.markerId.value.toString() == stay.id).first;

            Marker selectedMarker = markerToRemove.copyWith(
                iconParam: stay.customIconSelected, zIndexParam: 1);

            //remove the old one
            Stay unselectedStay = http.fetchedStays.asMap()[selectedPage];
            Marker markerUnselected = _markers.where((o) =>
            o.markerId.value.toString() == unselectedStay.id).first;
            Marker toAdd = markerUnselected.copyWith(
                iconParam: unselectedStay.customIcon);

            _markers.remove(markerUnselected);
            _markers.add(toAdd);

            setState(() {
              selectedPage = index;
              _markers.remove(markerToRemove);
              _markers.add(selectedMarker);
            });


          });
        }
      }
    });

  }


  @override
  void dispose() {
    animationController.dispose();
    pageController.dispose();
    focusPlaceNode.dispose();
    subscription.cancel();

    super.dispose();
  }



  When selectedDate = new When();
  setSelectedDate(When when)  {
    selectedDate = when;
    setState(() {
      pageController.animateToPage(0, duration: Duration(seconds: 1, milliseconds: 500), curve: ElasticOutCurve());
    });
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding:false,
      body:  Stack(
        children: <Widget>[
          buildPage(context),
        ],
      ) ,

    );
  }




  Widget buildPage(BuildContext context) {
    return PageView(
      controller: pageController,
      pageSnapping: true,
      physics:new NeverScrollableScrollPhysics(),
      scrollDirection: Axis.vertical,
      children: <Widget>[
        Container(
          child: buildFormWhere(),
        ),
        Container(
          child: buildFormWhen(),
        ),

      ],
    );

  }


  void isVibrate() async {
    canVibrate = await Vibration.hasVibrator().then((value) => canVibrate = true);
  }

  //ANIMATED PROPERTIES
  double removeHeight = 1;
  double numberHeight =  1;
  double addHeight = 1;

  double mapHeight = double.infinity;
  double buttonsBottom = 30;
  double searchButtonBottom = 27;


  bool searchPressed = false;

  Widget buildFormWhere(){

    return Container(
        margin: EdgeInsets.only(top: marginTop),
        height: 500,
        width: 100,
        child: Stack(
            children: <Widget>[

              mapVisible ?
              buildGoogleMaps() :
              Container(),
              buildWhere(),

              Positioned(
                top: MediaQuery.of(context).size.height * 0.665,
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.33,
                child: buildStayList(),
              ),

              ///BUTTONS

              /// SEARCH
              Positioned(
                bottom: searchButtonBottom,
                right: 10,
                child: mapVisible ? MaterialButton(
                  height: 43,
                  elevation: 5,
                  child: Text("Search", style: Theme.of(context).textTheme.display1),
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(Constants.radius),
                  ),
                  color: Theme.of(context).primaryColor,
                  disabledColor: Colors.grey,
                  disabledTextColor: Colors.white,
                  disabledElevation: 0,

                  onPressed: searchPressed ? null : () async {

                    mapHeight = MediaQuery.of(context).size.height * 0.66;
                    buttonsBottom = MediaQuery.of(context).size.height * 0.33;
                    searchButtonBottom = MediaQuery.of(context).size.height * 0.33;

                    //searchPressed = true;

                    try {
                      Dialogs.showLoadingDialog(context, _keyLoader, false);//invoking login
                      await getAccommodations();
                      Navigator.of(_keyLoader.currentContext,rootNavigator: true).pop();//close the dialoge
                      //Navigator.pushReplacementNamed(context, "/home");
                    } catch (error) {
                      print(error);
                    }

                  },
                ) : Container(),
              ),

              //CALENDAR
              mapVisible ? Positioned(
                  bottom: buttonsBottom,
                  left: 100,
                  height: 43,
                  child: buildNavigateButton(1, Icons.calendar_today)) : Container(),

              ////////// POSITION
              mapVisible ?
              Positioned(
                  bottom: buttonsBottom,
                  left:10,
                  height: 42,
                  child:buildMyLocation(Icons.my_location)) : Container(),

              mapVisible ? Positioned(
                  bottom: buttonsBottom,
                  left: 155,
                  height: 43,
                  child: buildRoomsGuests(Icons.person)) : Container(),



              /// FILTERS
              mapVisible ? Positioned(
                  bottom: buttonsBottom,
                  left: 210,
                  height: 43,
                  child: buildFilterButton(CustomIcon.MyFlutterApp.sliders)) : Container(),

            ])
    );
  }




  Future getAccommodations() async {
    setState(() {
      choose = false;
    });

    http.lat = _lastMapPosition.latitude;
    http.lng = _lastMapPosition.longitude;
    http.checkout =  selectedDate.selectedDateLast != null ? formatChosenDate(selectedDate.selectedDateLast) : formatChosenDate(DateTime.now().add(Duration(days:1)));
    http.checkin = selectedDate.selectedDateFirst != null ? formatChosenDate(selectedDate.selectedDateFirst) :   formatChosenDate(DateTime.now());
    http.adults = adults;
    http.children = children;
    http.min = _from;
    http.max = _to;
    http.rooms = rooms;

    http.hotels = hotels;
    http.apartments = apartments;
    http.hostels = hostels;

    http.onTap = _onTapMarker;
    http.hotelRating = hotelRating;
    http.airbnbRating = airbnbRating;

    http.currencyCode = currencyCode;
    http.coordinatesBound = coordinatesBound;

    print("HOME ");
    print("checking "+http.checkin);
    print("checkout "+http.checkout);
    print("party "+http.adults.toString());
    print("min "+http.min);
    print("max "+http.max);

    storeSearch();

    await http.fetchAccommodations();
    http.getMarkerList()
        .then((markers) {
      setState(() {

        _markers.clear();
        _markers.addAll(markers);
        choose = true;
        selectedPage = 0;

        focusPlaceNode.unfocus();

      });
    });
  }

  Future storeSearch() async {
    String userId = prefs.getString("userId");
    Search search = new Search(_lastMapPosition.latitude.toString(), _lastMapPosition.longitude.toString(),
        http.checkin, http.checkout, rooms, adults, children, _from, _to, hostels, apartments, hostels, hotelRating, airbnbRating
    );

    final QuerySnapshot result =
    await Firestore.instance.collection('users').where('id', isEqualTo: userId).getDocuments();
    final List<DocumentSnapshot> documents = result.documents;
    if (documents.length > 0) {
      Searches searchesList;
      if(documents[0]['searches'] != null){
        searchesList = Searches.fromMap(jsonDecode(documents[0]['searches']));
        searchesList.searches.add(search);
      }
      else {
        searchesList = new Searches([search]);
      }
      Firestore.instance
          .collection('users')
          .document(userId)
          .setData({'searches': jsonEncode(searchesList)}, merge: true);
    }
  }


  LatLngBounds coordinatesBound;

  Widget buildGoogleMaps() {
    return AnimatedOpacity(
        opacity: mapVisible ? 1.0 : 0.0,
        duration: Duration(seconds: 10),
        child :Container(
            width: double.infinity,
            height: mapHeight,
            child: GoogleMap(
                onCameraMove: ((_position) {
                  _mapController.getVisibleRegion().then( (onValue) => coordinatesBound = onValue);
                  _updatePosition(_position);
//                  setState(() {
//                    searchPressed = false;
//                  });
                }) ,
                gestureRecognizers: Set()
                  ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer()))
                  ..add(Factory<ScaleGestureRecognizer>(() => ScaleGestureRecognizer()))
                  ..add(Factory<TapGestureRecognizer>(() => TapGestureRecognizer()))
                  ..add(Factory<VerticalDragGestureRecognizer>(
                          () => VerticalDragGestureRecognizer())),
                markers: _markers,
                myLocationEnabled: true,
                myLocationButtonEnabled: false,
                mapType: MapType.normal,
                onMapCreated: (GoogleMapController controller) {
                  _mapController = controller;
                  _mapController.getVisibleRegion().then( (onValue) => coordinatesBound = onValue);
                },
                initialCameraPosition: CameraPosition(
                  target: _lastMapPosition,
                  zoom: 15.0,
                ))

        )

    );
  }

  void _updatePosition(CameraPosition _position) {
    _lastMapPosition = LatLng(_position.target.latitude, _position.target.longitude);
  }


  //double page = 160;
  int selectedPage = 0;
  //double scroll = 0;


  final ItemScrollController itemScrollController = ItemScrollController();
  final ItemPositionsListener itemPositionListener = ItemPositionsListener.create();


  _onTapMarker(var hotelId) {

    int index = http.fetchedStays.indexWhere( (o) =>
    o.id.toString() == hotelId.toString());



    if(index > -1) {
      itemScrollController.jumpTo(index: index);

      futureStays.then( (onValue) {
        Stay stay = onValue.firstWhere( (o) => o.id.toString() == hotelId.toString());
        stay.isSelected = true;

        Marker markerToRemove = _markers.where((o) =>
        o.markerId.value.toString() == stay.id).first;

        Marker selectedMarker = markerToRemove.copyWith(iconParam: stay.customIconSelected, zIndexParam: 1);

        //remove the old one
        Stay unselectedStay = http.fetchedStays.asMap()[selectedPage];
        Marker markerUnselected = _markers.where((o) =>
        o.markerId.value.toString() == unselectedStay.id).first;
        Marker toAdd = markerUnselected.copyWith(
            iconParam: unselectedStay.customIcon);

        _markers.remove(markerUnselected);
        _markers.add(toAdd);

        setState(() {
          selectedPage = index;
          _markers.remove(markerToRemove);
          _markers.add(selectedMarker);

        });

      });
    }


  }


  Future<List<Stay>> futureStays;
  Widget buildStayList() {
    futureStays  = http.getStayList();

    if(choose) return
      FutureBuilder<List<Stay>>(
        future: futureStays,
        builder: (context, snapshot) {

          if (snapshot.hasError) print(snapshot.error);
          if (!snapshot.hasData && snapshot.connectionState == ConnectionState.waiting)
            return buildShimmer();
          if (!snapshot.hasData || snapshot.data.isEmpty) {
            return buildNoResult();
          }
          return ScrollablePositionedList.builder(
              itemCount: snapshot.data.length,
              itemScrollController: itemScrollController,
              itemPositionsListener: itemPositionListener,
              key: ObjectKey(snapshot.data[0].id),
              //controller: _controller,
              scrollDirection: Axis.horizontal,
              physics: BouncingScrollPhysics(),
              //shrinkWrap: true,
              padding: EdgeInsets.only(left: 5.0, right: 5.0, top:5.0),
              //itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index)  {

                Stay stay = snapshot.data[index];
                if( selectedPage != null && selectedPage == index) {
                  stay.isSelected = true;

                  Marker selectedMark = _markers.firstWhere((o) =>
                  o.markerId.value.toString() == stay.id, orElse: () => null);

                  if(selectedMark != null) {
                    _mapController.animateCamera(
                        CameraUpdate.newCameraPosition(
                          CameraPosition(
                              target: selectedMark.position, zoom: 14.6),
                        ));
                  }
                }
                else {
                  stay.isSelected = false;
                }
                return stay;

              }
          );

        },
      );
    else return  Container();
  }

  Widget buildNoResult() {
    return Center(
      child: AutoSizeText("No result for your search. \nChange city or filters and try again.", maxLines: 2,
        textAlign: TextAlign.center,
        minFontSize: 20,maxFontSize: 22, style: TextStyle
        (

          color: AppColors.blue, fontFamily: "Arboria",
      ),
      ),
    );
  }

  Shimmer buildShimmer() {
    return Shimmer.fromColors(
        baseColor: Colors.grey[400],
        highlightColor: Colors.white,
        direction: ShimmerDirection.rtl,
        child: Column(
          children: [0]
              .map((_) => Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: 280,
                  height: MediaQuery.of(context).size.height * 0.15,
                  color: Colors.white,
                ),
                Padding(
                  padding:
                  const EdgeInsets.symmetric(vertical: 8.0),
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width * 0.3,
                        height: 10.0,
                        color: Colors.white,
                      ),
                      Padding(
                        padding:
                        const EdgeInsets.symmetric(vertical: 4.0),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.3,
                        height: 10.0,
                        color: Colors.white,
                      ),
                      Padding(
                        padding:
                        const EdgeInsets.symmetric(vertical: 4.0),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.3,
                        height: 10.0,
                        color: Colors.white,
                      ),
                      Padding(
                        padding:
                        const EdgeInsets.symmetric(vertical: 4.0),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.18,
                        height: 10.0,
                        color: Colors.white,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ))
              .toList(),
        ));
  }



  ///////////////////////////// MAPS ////////////////////////////////////////////////
  ///////////////////////////// MAPS ////////////////////////////////////////////////
  ///////////////////////////// MAPS ////////////////////////////////////////////////
  ///////////////////////////// MAPS ////////////////////////////////////////////////


  GoogleMapController _mapController;
  PlacesDetailsResponse placeDetail;

  String city;

  double lat;
  double lng;

  Set<Marker> _markers = {};
  LatLng _lastMapPosition = const LatLng(51.508530, -0.076132);

  GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: Constants.kGoogleApiKey);


  Future<Null> displayPrediction(Prediction p) async {
    if (p != null) {
      // get detail (lat/lng)
      placeDetail = await _places.getDetailsByPlaceId(p.placeId);
      lat = placeDetail.result.geometry.location.lat;
      lng = placeDetail.result.geometry.location.lng;

      city = placeDetail.result.formattedAddress;
      setState(() {
        choose = false; //reset the stay list;

        _lastMapPosition = new LatLng(lat, lng);
//        Marker marker = generateMarker();

        _markers.clear();
//        _markers.add(marker);

        _mapController.animateCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(
                target: _lastMapPosition, zoom: 14.6),
          ),
        );

        //reset the map view
        mapHeight = double.infinity;
        buttonsBottom = 30;
        searchButtonBottom = 27;

        focusPlaceNode.unfocus();
      });
    }
  }

  void onError(PlacesAutocompleteResponse response) {
    print("error");
  }


  /////////////////////////////////////////////////////////////////////////////


  double marginTop = 10;

  Widget buildWhere() {
    return AnimatedContainer(
      duration: Duration(seconds: 2),
      curve: Curves.elasticInOut,
      margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 30.0),
      child:  PlacesAutocompleteWidget(
        hint: city != null ? city : "Try London",
        apiKey: Constants.kGoogleApiKey,
        mode: Mode.overlay,
        language: "en",
        onPressed:  (p) {
          displayPrediction(p);
        },
        focusNode: focusPlaceNode,
        validate: false,
      ),

    );

  }


  Widget buildNavigateButton(int page, IconData icon) {
    return  Material (
        shape: new CircleBorder(),
        elevation: 5.0,
        child: IconButton(
          color: Theme.of(context).primaryColor,
          icon:Icon(
            icon,
            size: 20.0,
            //color: Colors.white,
          ),
          onPressed: () => pageController.animateToPage(page, duration: Duration(seconds: 1, milliseconds: 500), curve: ElasticOutCurve()),
        )
    );
  }



  static final double iconButtonDimension = 32;
  static final double iconSize = 16;

  Widget buildPlusMinusGuest(IconData icon, void setState(ui.VoidCallback fn)) {
    return  Material (
        shape: new CircleBorder(),
        elevation: 5.0,
        child: new SizedBox(
            height: iconButtonDimension,
            width: iconButtonDimension,
            child: new IconButton(
              padding: new EdgeInsets.all(0.0),
              color: Theme.of(context).primaryColor,
              icon:Icon(
                icon,
                size: iconSize ,
              ),
              onPressed: () => {
                addRemoveGuests(icon, setState)
              },
            )
        ));
  }

  addRemoveGuests(IconData icon, void setState(ui.VoidCallback fn)) {
    setState(() {
      if(icon == Icons.add) adults = adults + 1;
      if(icon == Icons.remove)
        if(adults > 0) {
          adults = adults - 1;
        }
    });
  }


  Widget buildPlusMinusChildren(IconData icon,  void setState(ui.VoidCallback fn)) {
    return  Material (
        shape: new CircleBorder(),
        elevation: 5.0,
        child: new SizedBox(
            height: iconButtonDimension,
            width: iconButtonDimension,
            child: new IconButton(
              padding: new EdgeInsets.all(0.0),
              color: Theme.of(context).primaryColor,
              icon:Icon(
                icon,
                size: iconSize ,
              ),
              onPressed: () => {
                addRemoveChildren(icon, setState)
              },
            )
        ));
  }

  addRemoveChildren(IconData icon,  void setState(ui.VoidCallback fn)) {
    setState(() {
      if(icon == Icons.add) children = children + 1;
      if(icon == Icons.remove)
        if(children > 0) {
          children = children - 1;
        }
    });
  }

  Widget buildPlusMinusRoom(IconData icon,  void setState(ui.VoidCallback fn)) {
    return   Material (
        shape: new CircleBorder(),
        elevation: 5.0,
        child: new SizedBox(
            height: iconButtonDimension,
            width: iconButtonDimension,
            child: new IconButton(
              padding: new EdgeInsets.all(0.0),
              color: Theme.of(context).primaryColor,
              icon:Icon(
                icon,
                size: iconSize ,
              ),
              onPressed: () => {
                addRemoveRoom(icon, setState)
              },
            )
        ));
  }

  void addRemoveRoom(IconData icon,  void setState(ui.VoidCallback fn)) {
    return setState(() {
      if(icon == Icons.add) rooms = rooms + 1;
      if(icon == Icons.remove)
        if(rooms > 0) {
          rooms = rooms - 1;
        }
    });
  }



  Widget buildMyLocation(IconData icon) {
    return  Material (
        shape: new CircleBorder(),
        elevation: 5.0,
        child: IconButton(
            color: Theme.of(context).primaryColor,
            icon:Icon(
              icon,
              size: 20.0,
            ),
            onPressed: () async {
              await getLocation();
            }
        )
    );
  }

  Future getLocation() async {
    LocationData currentLocation;
    var location = new Location();
    try {
      currentLocation = await location.getLocation();
    } on Exception {
    }
    _mapController.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(
          bearing: 0,
          target: LatLng(currentLocation.latitude, currentLocation.longitude),
          zoom: 15.0,
        )));
  }


  bool hotels = true;
  bool apartments = true;
  bool hostels = false;
  String currencyCode = "GBP";
  String countryCode = "GB";

  double hotelRating = 0.0;
  double airbnbRating = 0.0;

  Widget buildFilterButton(IconData icon) {
    return  Material (
        shape: new CircleBorder(),
        elevation: 5.0,
        child: IconButton(
            color: Theme.of(context).primaryColor,
            icon:Icon(
              icon,
              size: 20.0,
              //color: Colors.white,
            ),
            onPressed: () => {
              showDialog(
                  context: context,
                  barrierDismissible: true,
                  builder: (BuildContext context) {
                    return StatefulBuilder(
                        builder: (context, setState) {
                          return SimpleDialog(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(Constants.radius),
                              ),
                              backgroundColor: Colors.white,
                              children: <Widget>[
                                SizedBox(height: 10,),

                                Align(
                                  alignment: Alignment.center,
                                  child: Text("Currency", style:Theme.of(context).textTheme.body2.copyWith(fontSize: 20, color: AppColors.blue),),
                                ),

                                Padding(
                                  padding: EdgeInsets.only(left: 25, top: 20, bottom:30),
                                  child: CurrencyPickerDropdown(
                                    initialValue: countryCode,
                                    itemFilter:   (c) => [ 'EU', 'US', 'GB', 'CH'].contains(c.isoCode),
                                    itemBuilder: _buildDropdownItem,
                                    onValuePicked: (Country country) {
                                      setState(() {
                                        currencyCode = country.currencyCode;
                                        countryCode = country.isoCode;
                                      });
                                    },
                                  ),
                                ),


                                Align(
                                  alignment: Alignment.center,
                                  child: Text("Price range", style:Theme.of(context).textTheme.body2.copyWith(fontSize: 20, color: AppColors.blue),),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 25, top: 20, bottom:20),
                                  child: Text(currencySymbol.simpleCurrencySymbol(currencyCode)+" "+_from+" - "+
                                      currencySymbol.simpleCurrencySymbol(currencyCode)+" "+ ( _to == "500" ? "500+" : _to),
                                    style:TextStyle(fontFamily: "Arboria", fontSize: 16, color: AppColors.blue) ,),
                                ),
                                buildMoneySlider(setState),
                                SizedBox(height: 30,),

                                Align(
                                  alignment: Alignment.center,
                                  child: Text("Accommodation ", style:Theme.of(context).textTheme.body2.copyWith(fontSize: 20, color: AppColors.blue),),
                                ),
                                SizedBox(height: 10,),

                                CheckboxListTile(
                                  title: Text("Hotels",
                                      style:TextStyle(fontSize: 16, color: Colors.black54)),
                                  value: hotels,
                                  onChanged: (newValue) {
                                    setState(() {
                                      hotels = !hotels;
                                    });
                                  },
                                  controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
                                ),

                                Padding(
                                    padding: EdgeInsets.only(left: 66),
                                    child: Row(
                                      children: <Widget>[
                                        Text("Min: "),
                                        SmoothStarRating(
                                            allowHalfRating: false,
                                            onRatingChanged: (v) {
                                              hotelRating = v;
                                              setState(() {});
                                            },
                                            starCount: 5,
                                            rating: hotelRating,
                                            size: 26.0,
                                            filledIconData: Icons.star,
                                            halfFilledIconData: Icons.star,
                                            color: AppColors.blue,
                                            borderColor: AppColors.blue,
                                            spacing:0.0
                                        ),
                                      ],
                                    )

                                ),

                                CheckboxListTile(
                                  title: Text("Entire Appartments",
                                      style:TextStyle(fontSize: 16, color: Colors.black54)),

                                  value: apartments,
                                  onChanged: (newValue) {
                                    setState(() {
                                      apartments = !apartments;
                                    });
                                  },
                                  controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
                                ),
                                Padding(
                                    padding: EdgeInsets.only(left: 66),
                                    child:Row(
                                      children: <Widget>[
                                        Text("Min: "),
                                        SmoothStarRating(
                                            allowHalfRating: false,
                                            onRatingChanged: (v) {
                                              airbnbRating = v;
                                              setState(() {});
                                            },
                                            starCount: 5,
                                            rating: airbnbRating,
                                            size: 26.0,
                                            defaultIconData: Icons.favorite_border,
                                            filledIconData: Icons.favorite,
                                            halfFilledIconData: Icons.favorite,
                                            color: AppColors.blue,
                                            borderColor: AppColors.blue,
                                            spacing:0.0
                                        ),
                                      ],

                                    )),

                                CheckboxListTile(
                                  title: Text("Hostels and shared Airbnbs",
                                      style:TextStyle(fontSize: 16, color: Colors.black54)),
                                  value: hostels,
                                  onChanged: (newValue) {
                                    setState(() {
                                      hostels = !hostels;
                                    });
                                  },
                                  controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
                                ),

                                SizedBox(height: 20,),

                                //BUTTONS

                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.only(left: 20,),
                                      child: FlatButton(
                                        onPressed: () {
                                          setState(() {
                                            _from = "0";
                                            _to = "300";
                                            hotels = true;
                                            apartments = true;
                                            hostels = false;
                                          });
                                        },
                                        child: Text("Reset", style:TextStyle(color:AppColors.blue, fontFamily: "Arboria", fontSize: 16)),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(right: 20,),
                                      child:FlatButton(
                                          padding: EdgeInsets.all(8.0),
                                          shape: RoundedRectangleBorder(
                                            borderRadius: new BorderRadius.circular(Constants.radius),
                                          ),
                                          color: AppColors.blue,
                                          child: Text("OK", style:TextStyle(color: Colors.white, fontFamily: "Arboria", fontSize: 16)),
                                          onPressed: () {
                                            //launch("https://www.booking.com/hotel/us/renaissance-new-york-midtown.en-gb.html?aid=1607597&checkin=2020-08-27&checkout=2020-08-28&interval=1&label=stay22_ctrl035-desktop%2Aen%2Ait%2Achrome%2A1223&room1=A&selected_currency=EUR", forceSafariVC: false);
//                                            launch("booking://hotels/278977?&selected_currency=EUR&room1=A%2CA&room2=A%2CA&checkin=2020-04-27&checkout=2020-04-28&");
                                            Navigator.pop(context);
                                          }),
                                    ),

                                  ],
                                ),

                              ]);
                        }
                    );

                  })
            }
        ));
  }

  NumberFormat currencySymbol = new NumberFormat();



  Widget _buildDropdownItem(Country country) => Container(
    child: Row(
      children: <Widget>[
        CurrencyPickerUtils.getDefaultFlagImage(country),
        SizedBox(
          width: 8.0,
        ),
        Text("${country.currencyCode}"),
      ],
    ),
  );



  //// Guests, rooms

  Widget buildRoomsGuests(IconData icon) {
    return  Material (
        shape: new CircleBorder(),
        elevation: 5.0,
        child: IconButton(
            color: Theme.of(context).primaryColor,
            icon:Icon(
              icon,
              size: 20.0,
              //color: Colors.white,
            ),
            onPressed: () => {
              showDialog(
                  context: context,
                  barrierDismissible: true,
                  builder: (BuildContext context) {
                    return StatefulBuilder(
                        builder: (context, setState) {
                          return SimpleDialog(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(Constants.radius),
                              ),
                              backgroundColor: Colors.white,
                              children: <Widget>[
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    SizedBox(height: 10,),
                                    Text("Guests", style:Theme.of(context).textTheme.body2.copyWith(fontSize: 20, color: AppColors.blue),),
                                    SizedBox(height: 30,),
                                    Text("Adults", style:Theme.of(context).textTheme.body2.copyWith(fontSize: 16, color: AppColors.blue),),

                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        buildPlusMinusGuest(Icons.remove, setState),
                                        Text(adults.toString(), style:Theme.of(context).textTheme.body2.copyWith(fontSize: 16, color: AppColors.blue),),
                                        buildPlusMinusGuest(Icons.add, setState),
                                      ],
                                    ),
                                    SizedBox(height: 20,),
                                    Text("Children", style:Theme.of(context).textTheme.body2.copyWith(fontSize: 16, color: AppColors.blue),),

                                    Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
                                          buildPlusMinusChildren(Icons.remove, setState),
                                          Text(children.toString(), style:Theme.of(context).textTheme.body2.copyWith(fontSize: 16, color: AppColors.blue),),
                                          buildPlusMinusChildren(Icons.add, setState),
                                        ]),
                                  ],
                                ),
                                SizedBox(height: 30,),

                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,

                                  children: <Widget>[
                                    Text("Rooms", style:Theme.of(context).textTheme.body2.copyWith(fontSize: 20, color: AppColors.blue),),

                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                                      children: <Widget>[
                                        buildPlusMinusRoom(Icons.remove, setState),
                                        Text(rooms.toString(), style:Theme.of(context).textTheme.body2.copyWith(fontSize: 16, color: AppColors.blue),),
                                        buildPlusMinusRoom(Icons.add, setState),

                                      ],
                                    ),
                                  ],
                                ),
                                SizedBox(height: 40,),

                                //BUTTONS

                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.only(left: 20,),
                                      child: FlatButton(
                                        onPressed: () {
                                          Navigator.pop(context);
                                          setState(() {

                                          });
                                        },
                                        child: Text("Reset", style:TextStyle(color:AppColors.blue, fontFamily: "Arboria", fontSize: 16)),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(right: 20,),
                                      child:FlatButton(
                                          padding: EdgeInsets.all(8.0),
                                          shape: RoundedRectangleBorder(
                                            borderRadius: new BorderRadius.circular(Constants.radius),
                                          ),
                                          color: AppColors.blue,
                                          child: Text("OK", style:TextStyle(color: Colors.white, fontFamily: "Arboria", fontSize: 16)),
                                          onPressed: () => {
                                            Navigator.pop(context),

                                          }),
                                    ),

                                  ],
                                ),

                              ]);
                        }
                    );

                  })
            }
        ));
  }









  String _from  = "0";
  String _to = "300";

  Widget buildMoneySlider(void setState(ui.VoidCallback fn)) {
    return Container(
      padding: EdgeInsets.only(left: 15, right: 15),
      child: RangeSlider(
        min: 0,
        max: 500,
        lowerValue: double.parse(_from),
        upperValue: double.parse(_to),
        showValueIndicator: false,
        valueIndicatorMaxDecimals: 0,
        onChanged: (double newLowerValue, double newUpperValue) {
          setState(() {
            _from = newLowerValue.toInt().toString();
            _to = newUpperValue.toInt().toString();

          });
        },
      ),
    );

  }

  Widget buildFormWhen() {
    return SafeArea(
        child: Stack(
          children: <Widget>[
            Container(
              //width: 500,
              child: SizedBox(
                height: MediaQuery.of(context).size.height,
                child: buildWhenButton(),
              ),
            ),
            Positioned(
                top: 10,
                left: 15,
                child: buildNavigateButton(0, Icons.map)),
          ],
        )
    );
  }

  String formatChosenDate(DateTime dateTime) {
    return formatDate(dateTime, [mm, '/', dd, '/', yyyy]);
  }


  Widget buildWhenButton() {
    return getDatePicker(
      context: context,
      onDateSelected:  (p) {
        setSelectedDate(p);
      },

      initialLastDate:  selectedDate.selectedDateLast != null ? selectedDate.selectedDateLast : (DateTime.now().add(Duration(days:1))),
      initialDate: selectedDate.selectedDateFirst != null ? selectedDate.selectedDateFirst :  DateTime.now(),
      firstDate: new DateTime.now().subtract(Duration(hours: 1)),
      lastDate: new DateTime(2050),
    );
  }


  void getUserLocation() async {
    var location = new Location();
    LocationData calculatedLocation = await location.getLocation();


    setState(() {
      // Platform messages may fail, so we use a try/catch PlatformException.
      currentLocation = calculatedLocation;
    });
  }


  final GlobalKey<State> _keyLoader = new GlobalKey<State>();


}

