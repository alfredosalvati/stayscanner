import 'package:flutter/material.dart';
import 'package:stayscanner/splash.dart';

import 'common/colors.dart';

void main() => runApp(MyApp());


class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return MaterialApp(
      debugShowCheckedModeBanner: false,

//      appBar: AppBar(
//        // Here we take the value from the MyHomePage object that was created by
//        // the App.build method, and use it to set our appbar title.
//        title: Text(widget.title),
//      ),
      theme:  ThemeData(
        // Define the default Brightness and Colors
          brightness: Brightness.light,
          primaryColor: AppColors.blue,
          accentColor: Colors.grey,
          dividerColor: Colors.white,
          buttonColor: Colors.white,
          backgroundColor:Colors.white ,
          splashColor: Colors.white,


          // Define the default Font Family
          fontFamily: 'Arboria',

          cursorColor: Colors.grey,

          // Define the default TextTheme. Use this to specify the default
          // text styling for headlines, titles, bodies of text, and more.
          textTheme: TextTheme(
            subhead: TextStyle(fontSize: 20,),
            body2: TextStyle(fontSize: 14, color: Colors.white, fontWeight: FontWeight.bold),
            display2: TextStyle(fontSize: 14, color: Colors.white, fontWeight: FontWeight.normal),
            headline: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
            title: TextStyle(fontSize: 36.0, color: Colors.white),
            body1: TextStyle(fontSize: 14.0, fontFamily: 'Arboria', color: AppColors.blue),
            display1: TextStyle(fontSize: 18.0, color: Colors.white),
          )),
      home:SplashScreen(), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
