import 'package:flutter/material.dart';

class Constants extends Object {

  static const double radiusCorners = 100.0;
  static const double radiusCornersCalendar = 30.0;

  static const kGoogleApiKey = "AIzaSyCbV0p0youjKlA_67EM6BeQKQqWNqk6x3M";

  static const Color bookingColor = const Color(0xff07487F); // Second `const` is optional in assignments.
  static const Color airbnbColor = const Color(0xffFC5C62); // Second `const` is optional in assignments.

  static const double pageStayWidht = 300.0;
  static const double pageStayHeight = 220.0;

  static const double radius = 15.0;

}