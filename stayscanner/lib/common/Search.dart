
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';


class Searches {

  List<Search> searches;

  Searches(this.searches);

//  Searches.fromMap(List<dynamic> json){
//    searches = new List<Search>();
//    for(var item in json ) {
//      searches.add(new Search.fromJson(item));
//    }
//
//  }

  Searches.fromMap(Map<String, dynamic> json ){
    if (json['searches'] != null) {
      searches = new List<Search>();
      json['searches'].forEach((v) {
        searches.add(new Search.fromJson(v));
      });
    }
  }

  Map toJson() => {
    "searches": searches
  };

}


class Search {

  String lng;
  String lat;

  String checkin;
  String checkout;

  int rooms;
  int adults;
  int children;

  String min;
  String max;
  bool hotels;
  bool apartments;
  bool hostels;

  double hotelRating;
  double airbnbRating;


  Search(this.lat, this.lng, this.checkin, this.checkout, this.rooms,
      this.adults, this.children, this.min, this.max, this.hotels,
      this.apartments, this.hostels, this.hotelRating, this.airbnbRating);


  Search.fromJson(Map<String, dynamic> json) {
    checkin = json['checkin'];
    checkout = json['checkout'];
    rooms = json['rooms'];
    adults = json['adults'];
    children = json['children'];
    min = json['min'];
    max = json['max'];
    hotels = json['hotels'];
    apartments = json['apartments'];
    hostels = json['hostels'];
    hotelRating = json['hotelRating'];
    airbnbRating = json['airbnbRating'];
    lat = json['lat'];
    lng = json['lng'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['checkin'] = this.checkin;
    data['checkout'] = this.checkout;
    data['rooms'] = this.rooms;
    data['adults'] = this.adults;
    data['children'] = this.children;
    data['min'] = this.min;
    data['max'] = this.max;
    data['hotels'] = this.hotels;
    data['apartments'] = this.apartments;
    data['hostels'] = this.hostels;
    data['hotelRating'] = this.hotelRating;
    data['airbnbRating'] = this.airbnbRating;
    data['lat'] = lat;
    data['lng'] = lng;
    return data;
  }
}