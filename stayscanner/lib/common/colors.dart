import 'dart:ui' show Color;


class AppColors extends Object {

  // Brand Colors

  static const Color red = const Color(0xffA63C3C);
  static const Color beige = const Color(0xffF2CF9B);
  static const Color orange = const Color(0xffF2A057);

  //STEFANO COLORS
  static const Color green = const Color(0xff72CBC2);
  static const Color darkGreen = const Color(0xff478289);


  static const Color blue = const Color(0xff5bc6f6);



  static const Color bubbleRed = const Color.fromRGBO(243, 37, 88, 1.0);
  static const Color bubblePurple = const Color.fromRGBO(120, 28, 239, 1.0);






  ////// ADAM

  // Grays
  static const Color textGray = const Color.fromRGBO(116, 122, 142, 1.0);
  static const Color textLightGray = const Color.fromRGBO(137, 167, 182, 1.0);
  static const Color textLighterGray = const Color.fromRGBO(164, 170, 188, 1.0);
  static const Color textVeryLightGray = const Color.fromRGBO(176, 197, 208, 1.0);


  // Gradient
  static const Color gradientStartOrange = const Color.fromRGBO(255, 106, 0, 1.0);
  static const Color gradientEndPink = const Color.fromRGBO(239, 15, 114, 1.0);


  // Shimmer
  static const Color shimmerBase = const Color.fromRGBO(215, 215, 215, 1.0);
  static const Color shimmerHighlight = const Color.fromRGBO(240, 240, 240, 1.0);

// Misc


}