import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stayscanner/common/colors.dart';
import 'home.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_auth/firebase_auth.dart';



class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  SharedPreferences prefs;

  Firestore dbAccess;

  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  Future<FirebaseUser> signInAnon() async {
    AuthResult result = await firebaseAuth.signInAnonymously();
    FirebaseUser user = result.user;
    print("Signed in: ${user.uid}");
    return user;
  }
  void signOut() {
    firebaseAuth.signOut();
    print('Signed Out!');
  }

  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance().then( (value) => prefs = value);
    signInAnon().then((FirebaseUser user){
      print('Login success!');
      print('UID: ' + user.uid);
      dbAccess = Firestore.instance;

    });

    Timer(Duration(microseconds: 1000), () {

      setState(() {
        opacity = 1.0;
      });
      storeInfoFireBase();

    });



    Timer(
        Duration(seconds: 2),
            () => Navigator.of(context).push(
            PageRouteBuilder(
              pageBuilder: (context, animation1, animation2) {
                return Home();
              },
              transitionsBuilder: (context, animation1, animation2, child) {
                return FadeTransition(
                  opacity: animation1,
                  child: child,
                );
              },
              transitionDuration: Duration(milliseconds: 1000),
            )));
  }

  double opacity = 0.0;

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        backgroundColor: AppColors.blue,
        body: Container(
          child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(bottom: 30),
                    child: Image.asset('img/StayScannerLogo.png',
                      width: MediaQuery.of(context).size.width*0.55,
                      //height: MediaQuery.of(context).size.height*0.4,
                    ),
                  ),
                  AnimatedOpacity(
                    opacity: opacity,
//                curve: Curves.elasticInOut,
                    duration: Duration(milliseconds: 2000),
                    child: Text("Stayscanner", style: TextStyle(
                        fontSize: 42,
                        color: Colors.white,
                        fontFamily: "Rounded-Elegance",
                        fontWeight: FontWeight.normal
                    ),
                    ),
                  )
                ],
              )
          ),
        )
    );
  }

  void storeInfoFireBase() async {
    String userId;
    LocationData locationData;

    //TODO: place this some where else
    Future.wait([_getId(), getUserLocation()])
    .then( (onValue)=> {
      userId = onValue.first,
      locationData = onValue.last,
      prefs.setString('userId', userId)
    })
    .catchError((onError) => print(onError.toString()))
        .whenComplete(() {

          print("SPLASH Information \nDevice "+userId+"+\nLocation \nlatitude: "+locationData.latitude.toString()+" longitued: "+locationData.longitude.toString());
      GeoPoint location = new GeoPoint(locationData.latitude, locationData.longitude );


      dbAccess.runTransaction((Transaction transactionHandler) {
        return Firestore.instance
            .collection("users")
            .document(userId)
            .setData({
          "id": userId,
          "location": location,
          "lastopened:": DateTime.now(),
        },merge: true);
      });
    });
    
  }

  Future<LocationData> getUserLocation() async {
    var location = new Location();
    LocationData calculatedLocation = await location.getLocation();
    return calculatedLocation;
  }



  Future<String> _getId() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Theme.of(context).platform == TargetPlatform.iOS) {
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }


}