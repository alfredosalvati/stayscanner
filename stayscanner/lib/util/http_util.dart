


import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:stayscanner/dto/Booking.dart';
import 'package:stayscanner/dto/Hotel.dart';
import 'package:stayscanner/dto/Response.dart';
import 'package:stayscanner/util/stay.dart';
import 'package:stayscanner/dto/Airbnb.dart';
import 'package:stayscanner/util/marker_maker.dart';

class HttpUtil {

  ValueSetter onTap;

  double zoom = 15;

  String currencyCode;

  double lat;
  double lng;
  String checkin;
  String checkout;
  String min;
  String max;

  bool hotels;
  bool apartments;
  bool hostels;

  double hotelRating;
  double airbnbRating;

  int rooms;
  int adults;
  int children;

  LatLngBounds coordinatesBound;

  List<Stay> fetchedStays;


  static Map<String, String> requestHeaders = {
    'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/82.0.4060.0 Safari/537.36',
    'Accept':'text/javascript, application/javascript, application/ecmascript, application/x-ecmascript, */*; q=0.01',
    'Accept-Encoding':'gzip, deflate',
    'sec-fetch-mode':'cors',
    'Access-Control-Allow-Origin':'*'

  };

  String booking_api_url = "https://www.stay22.com/api/booking?&did=stayscanner&cid=oe&aid=stayscanner&legacyaid=stayscanner&isnear=true&isnear=true&isstatic=false&isinstantbook=false&islastminuterequest=false&timezone=-05%3A00&priceper=nightly&langshort=en&limit=14&featuredhcids=&featuredhpids=&skiphcids=&skipabids=&onlyhcids=&onlyabids=&abtest=ctrl035&localairtld=.com&impactid=436504&&";
  String hotel_combined = "https://www.stay22.com/api/hotelscombined?&did=stayscanner&cid=oe&aid=stayscanner&legacyaid=stayscanner&isnear=true&isnear=true&isstatic=false&isinstantbook=false&islastminuterequest=false&timezone=-05%3A00&priceper=nightly&langshort=en&limit=14&featuredhcids=&featuredhpids=&skiphcids=&skipabids=&onlyhcids=&onlyabids=&abtest=ctrl035&localairtld=.com&impactid=436504&&";
  String airbnb_api_url = "https://www.stay22.com/api/airbnb?&did=stayscanner&cid=gm&aid=stayscanner&legacyaid=stayscanner&isnear=true&isstatic=false&isinstantbook=false&islastminuterequest=false&timezone=-05%3A00&priceper=nightly&langshort=en&limit=14&featuredhcids=&featuredhpids=&skiphcids=&skipabids=&onlyhcids=&onlyabids=&abtest=ctrl035&localairtld=.com&impactid=436504&&";



  Future<Booking> getBooking() async {

    double nelat = coordinatesBound.northeast.latitude;
    double nelng = coordinatesBound.northeast.longitude;

    double swlat = coordinatesBound.southwest.latitude;
    double swlng = coordinatesBound.southwest.longitude;

    if(max == "500")
      max = "10000";

    String minRating = "";
    if(hotelRating > 0){
      minRating="&minstarrating="+hotelRating.toInt().toString();
    }

    String info = getInfoString(nelat, nelng, swlat, swlng, minRating);

    print("Coordinates centerlat:"+lat.toString()+", "+lng.toString());
    print("Coordinates nelat:"+nelat.toString()+","+nelng.toString());
    print("Coordinates swlat:"+swlat.toString()+", "+swlng.toString());
    print("HTTP BOOKING "+booking_api_url+info+"\n\n");

    String finalUrl = booking_api_url+info;

    //http.get(finalUrl, headers:requestHeaders );
    try{
      var response =
      await http.get(finalUrl, headers:requestHeaders );
      if (response.statusCode == 200) {
        Booking hotel = parseBooking(response.body);
        print("Results for hotels: "+hotel.amtOfResults.toString());
        if(hotel.amtOfResults == 0) {
          response =
          await http.get(finalUrl, headers:requestHeaders );
          Booking hotel2 = parseBooking(response.body);
          print("Results for hotels second call: "+hotel2.amtOfResults.toString());
          return hotel2;
        }
        print("RETURNING HOTELS");
        return hotel;
      } else {
        print("impossible to fetch Bookings, response: "+response.statusCode.toString());
        return null;
      }
    } catch(e, stacktrace) {
      print(e.toString());
      print(stacktrace.toString());
    }
  }


  Future<Hotel> getHotelCombined() async {

    double nelat = coordinatesBound.northeast.latitude;
    double nelng = coordinatesBound.northeast.longitude;

    double swlat = coordinatesBound.southwest.latitude;
    double swlng = coordinatesBound.southwest.longitude;

    if(max == "500")
      max = "10000";

    String minRating = "";
    if(hotelRating > 0){
      minRating="&minstarrating="+hotelRating.toInt().toString();
    }

    String info = getInfoString(nelat, nelng, swlat, swlng, minRating);

//    print("Coordinates centerlat:"+lat.toString()+", "+lng.toString());
//    print("Coordinates nelat:"+nelat.toString()+","+nelng.toString());
//    print("Coordinates swlat:"+swlat.toString()+", "+swlng.toString());
    print("HTTP HOTELSCOMB "+hotel_combined+info+"\n\n");

    String finalUrl = hotel_combined+info;

    try {



      var response =
      await http.get(finalUrl, headers:requestHeaders );
      if (response.statusCode == 200) {
        Hotel hotel = parseHotel(response.body);
        print("Results for HOTELSCOMB: "+hotel.amtOfResults.toString());
//      if(hotel.amtOfResults == 0) {
//        response =
//        await http.get(finalUrl, headers:requestHeaders );
//        Booking hotel2 = parseBooking(response.body);
//        print("Results for hotels second call: "+hotel2.amtOfResults.toString());
//        return hotel2;
//      }
//      print("RETURNING HOTELS");
        return hotel;
      } else {
        print("impossible to fetch Bookings, response: "+response.statusCode.toString());
        return null;
      }
    } catch(e, stacktrace) {
      print(e.toString());
      print(stacktrace.toString());
    }
  }

  String getInfoString(double nelat, double nelng, double swlat, double swlng, String minRating) {
    String info = 'lat='+lat.toString()+'&lng='+lng.toString()+'&'
        'centerlat='+lat.toString()+'&centerlng='+lng.toString()+'&nelat='+nelat.toString()+'&'
        'nelng='+nelng.toString()+'&swlat='+swlat.toString()+'&swlng='+swlng.toString()+'&_=1577112540627&checkin='+checkin+'&checkout='+checkout
        +"&adults="+adults.toString()+"&children="+children.toString()+"&infants=0&guests="+(adults+children).toString()+"&min="+min+"&max="+max+"&showhotels="+hotels.toString()
        +"&showairbnbs="+apartments.toString()+"&showothers="+hostels.toString()+"&rooms="+rooms.toString()+"&currency="+currencyCode+minRating;
    return info;
  }


  Future<Airbnb> getAirbnb() async {

    double nelat = coordinatesBound.northeast.latitude;
    double nelng = coordinatesBound.northeast.longitude;

    double swlat = coordinatesBound.southwest.latitude;
    double swlng = coordinatesBound.southwest.longitude;

    if(max == "500")
      max = "10000";

    String minguestrating = "";
    if(airbnbRating > 0)
      minguestrating = "&minguestrating="+airbnbRating.toInt().toString();

    String info = getInfoString(nelat, nelng, swlat, swlng, minguestrating);

    print("HTTP AIRBNB "+airbnb_api_url+info+"\n\n");

    http.get(airbnb_api_url+info, headers: requestHeaders);

    try {
      var response =
      await http.get(airbnb_api_url+info, headers: requestHeaders);
      Airbnb airbnb = parseAirbnb(response.body);
      if (response.statusCode == 200) {
        print("Results for airbnb: "+airbnb.amtOfResults.toString());
        if(airbnb.amtOfResults == 0) {
          response =
          await http.get(airbnb_api_url+info, headers:requestHeaders );
          Airbnb airbnb2 = parseAirbnb(response.body);
          print("Results for airbnb second: "+airbnb2.amtOfResults.toString());
          return airbnb2;
        }
        return airbnb;
      } else {
        throw Exception('Unable to fetch airbnb from the REST API');
      }
    } catch(e, stacktrace) {
      print(e.toString());
      print(stacktrace.toString());
    }
  }

  Airbnb parseAirbnb(String responseBody) {
    // final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return Airbnb.fromJson(json.decode(responseBody));
  }

  Hotel parseHotel(String responseBody) {
    // final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return Hotel.fromJson(json.decode(responseBody));
  }

  Booking parseBooking(String responseBody) {
    // final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return Booking.fromJson(json.decode(responseBody));
  }

  Airbnb autogeneratedAirbnb;
  Booking autogeneratedBooking;
  Hotel autogenerdatedHotel;


  fetchAccommodations() async {

    List<Response> list = await Future.wait([getHotelCombined(), getBooking(), getAirbnb()]);

    autogenerdatedHotel = list[0];
    autogeneratedBooking = list[1];
    autogeneratedAirbnb = list[2];
  }



  Future<List<Marker>> getMarkerList() async {
    List<Marker> markers = new List<Marker>();
    int i = 0;

    if(autogeneratedBooking != null) {
      for (var f in autogeneratedBooking.results) {
        if(f.data.hotelProvider.code == "BKS") {
          String price = f.data.rates.first.totalF;

          MarkerMaker markerMaker = new MarkerMaker(
              f.latLng.first, f.latLng.last, f.hid,
              price, "booking", onTap, currencyCode);
          Marker marker;

          if (i == 0) {
            marker = await markerMaker.generateMarkerSelected();
          }
          else {
            marker = await markerMaker.generateMarker();
          }
          i = i + 1;
          markers.add(marker);
        }
      }
    }

    if(autogenerdatedHotel != null) {
      for (var f in autogenerdatedHotel.results) {
        if(f.data.hotelProvider.code == "BKS") {
          String price = f.data.rates.first.totalF;

          MarkerMaker markerMaker = new MarkerMaker(
              f.latLng.first, f.latLng.last, f.hid,
              price, "booking", onTap, currencyCode);
          Marker marker;

          if (i == 0) {
            marker = await markerMaker.generateMarkerSelected();
          }
          else {
            marker = await markerMaker.generateMarker();
          }
          i = i + 1;
          markers.add(marker);
        }
      }
    }

    if(autogeneratedAirbnb != null) {
      for (var f in autogeneratedAirbnb.results) {
        String price = f.data.rates.first.totalF;

        MarkerMaker markerMaker = new MarkerMaker(
            f.latLng.first, f.latLng.last, f.hid,
            price, "airbnb", onTap, currencyCode);
        Marker marker;
        if(i == 0) {
          marker = await markerMaker.generateMarkerSelected();
        }
        else {
          marker = await markerMaker.generateMarker();
        }
        i = i + 1;
        markers.add(marker);
      }
    }

    return markers;

  }

  Future<List<Stay>> getStayList() async {
    List<Stay> stays = new List<Stay>();
    if(autogeneratedBooking != null) {
      for (var f in autogeneratedBooking.results) {
        if (f.data.hotelProvider.code == "BKS") {
          String price = f.data.rates.first.totalF;

          Stay stay = new Stay(
              f.data.name,
              f.data.stars.toDouble(),
              null,
//              f.data.reviewCount,
              f.hid,
              price,
              "booking",
              f.data.rates[0].url,
              f.data.thumb,
              false,
              false,
              false,
              f.data.type,
              currencyCode);
          stays.add(stay);
          stay.generateIcons();
        }

      }
    }    if(autogenerdatedHotel != null) {
      for (var f in autogenerdatedHotel.results) {
        if (f.data.hotelProvider.code == "BKS") {
          String price = f.data.rates.first.totalF;

          Stay stay = new Stay(
              f.data.name,
              f.data.stars.toDouble(),
//null,
              f.data.reviewCount,
              f.hid,
              price,
              "booking",
              f.data.rates[0].url,
              f.data.thumb,
              false,
              false,
              false,
              f.data.type,
              currencyCode);
          stays.add(stay);
          stay.generateIcons();
        }
      }
    }
    if(autogeneratedAirbnb != null) {
      for (var f in autogeneratedAirbnb.results) {
        String price = f.data.rates.first.totalF;

        Stay stay = new Stay(
            f.data.name,
            f.data.rating != null && f.data.rating != "null" ? double.parse(f.data.rating) : 0.0,
            f.data.reviewCount,
            f.hid,
            price,
            "airbnb",
            f.data.urlDirect,
            f.data.thumb,
            f.data.isAirbnbPlus,
            f.data.isSuperHost,
            f.isInstantBook,
            f.data.type,
            currencyCode);
        stay.generateIcons();
        stays.add(stay);
      }
    }

    fetchedStays = stays;
    return stays;
  }


}