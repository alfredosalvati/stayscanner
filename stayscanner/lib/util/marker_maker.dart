import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart' hide TextDirection;
import 'dart:ui' as ui;
import 'dart:async';

import 'package:location/location.dart';
import 'package:stayscanner/common/constants.dart';


class MarkerMaker {
  final ValueSetter onTap;

  final double latitude;
  final double longitude;
  String id;
  final String price;
  final String type;
  final String currencyCode;

  NumberFormat currencySimbol = new NumberFormat();


  MarkerMaker(this.latitude, this.longitude, this.id, this.price, this.type,  this.onTap, this.currencyCode);


  Future<Marker> generateMarker() async {
    await createMarker();
    LatLng _lastMapPosition = new LatLng(latitude, longitude);
    Marker marker = Marker(
      // This marker id can be anything that uniquely identifies each marker.
      markerId: MarkerId(id),
      draggable: false,
      position: _lastMapPosition,
      icon: customIcon,
      onTap:() {
        onTap(id);
      }

    );
    return marker;
  }


  Future<Marker> generateMarkerSelected() async {
    await createMarkerSelected();
    LatLng _lastMapPosition = new LatLng(latitude, longitude);
    Marker marker = Marker(
      // This marker id can be anything that uniquely identifies each marker.
      markerId: MarkerId(id),
      draggable: false,
      flat: false,
      position: _lastMapPosition,
      icon: customIconSelected,
        onTap:() {
          onTap(id);
        }
    );
    return marker;
  }

  BitmapDescriptor customIcon;
  BitmapDescriptor customIconSelected;

  createMarker() async {
    customIcon = await getMarkerIcon(type, Size(200.0, 200.0));
  }

  createMarkerSelected() async {
    customIconSelected = await getMarkerIconSelected(type, Size(300.0, 300.0));
  }

  Future<BitmapDescriptor> getMarkerIcon(String imagePath, Size size) async {
    final ui.PictureRecorder pictureRecorder = ui.PictureRecorder();
    final Canvas canvas = Canvas(pictureRecorder);


    final double shadowWidth = 30.0;

    final Paint borderPaint = Paint()..color = type == "airbnb" ? Constants.airbnbColor : Constants.bookingColor;

    //final double imageOffset = shadowWidth + borderWidth;
    final double imageOffset = shadowWidth;

    canvas.drawRRect(
        RRect.fromRectAndCorners(
          Rect.fromLTWH(
              20,
              45,
              120,
              60
          ),
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10),
          bottomLeft: Radius.circular(10),
          bottomRight: Radius.circular(10),
        ),
        borderPaint);



    // Add tag text
    TextPainter textPainter = TextPainter(textDirection: TextDirection.ltr);
    textPainter.text = TextSpan(
      text: price+currencySimbol.simpleCurrencySymbol(currencyCode),
      style: TextStyle(fontSize: price.length > 3 ? 22.5 : 30.0, color: Colors.white, fontWeight: FontWeight.bold),
    );

    textPainter.layout();
    textPainter.paint(
        canvas,
        Offset(
            55,
            55
        )
    );

    // Oval for the image
    Rect oval = Rect.fromLTWH(
        0,
        0,
        size.width - (imageOffset * 2),
        size.height //- (imageOffset * 2)
    );

    // Add image
    ui.Image image = await getImage(type); // Alternatively use your own method to get the image
    paintImage(canvas: canvas, image: image, rect: oval, fit: BoxFit.fitWidth);

    // Convert canvas to image
    final ui.Image markerAsImage = await pictureRecorder.endRecording().toImage(
        size.width.toInt(),
        size.height.toInt()
    );

    // Convert image to bytes
    final ByteData byteData = await markerAsImage.toByteData(format: ui.ImageByteFormat.png);
    final Uint8List uint8List = byteData.buffer.asUint8List();

    return BitmapDescriptor.fromBytes(uint8List);
  }

  Completer<ImageInfo> completer = Completer();
  Future<ui.Image> getImage(String type) async {
    var img = new AssetImage("img/"+type+"_trans.png");

    if(!completer.isCompleted) {
      img.resolve(ImageConfiguration()).addListener(
          ImageStreamListener((ImageInfo info, bool _) {
            completer.complete(info);
          }));
    }
    ImageInfo imageInfo = await completer.future;
    return imageInfo.image;
  }


  Future<BitmapDescriptor> getMarkerIconSelected(String imagePath, Size size) async {
    final ui.PictureRecorder pictureRecorder = ui.PictureRecorder();
    final Canvas canvas = Canvas(pictureRecorder);


    final double shadowWidth = 30.0;

    final Paint borderPaint = Paint()..color = type == "airbnb" ? Constants.airbnbColor : Constants.bookingColor;

    //final double imageOffset = shadowWidth + borderWidth;
    final double imageOffset = shadowWidth;

    canvas.drawRRect(
        RRect.fromRectAndCorners(
          Rect.fromLTWH(
              20,
              45,
              215,
              115
          ),
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10),
          bottomLeft: Radius.circular(10),
          bottomRight:Radius.circular(10),
        ),
        borderPaint);



    // Add tag text
    TextPainter textPainter = TextPainter(textDirection: TextDirection.ltr);
    textPainter.text = TextSpan(
      text: price+currencySimbol.simpleCurrencySymbol(currencyCode),
      style: TextStyle(fontSize:  price.length > 3 ? 35.0 : 45.0, color: Colors.white, backgroundColor: Colors.transparent, fontWeight: FontWeight.bold),
    );

    textPainter.layout();
    textPainter.paint(
        canvas,
        Offset(
            100,
            80
        )
    );

    // Oval for the image
    Rect oval = Rect.fromLTWH(
        0,
        0,
        size.width - (imageOffset * 2),
        size.height //- (imageOffset * 2)
    );

    // Add image
    ui.Image image = await getImage(type); // Alternatively use your own method to get the image
    paintImage(canvas: canvas, image: image, rect: oval, fit: BoxFit.fitWidth);

    // Convert canvas to image
    final ui.Image markerAsImage = await pictureRecorder.endRecording().toImage(
        size.width.toInt(),
        size.height.toInt()
    );

    // Convert image to bytes
    final ByteData byteData = await markerAsImage.toByteData(format: ui.ImageByteFormat.png);
    final Uint8List uint8List = byteData.buffer.asUint8List();

    return BitmapDescriptor.fromBytes(uint8List);
  }
}