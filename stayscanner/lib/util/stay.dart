import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating/flutter_rating.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:shimmer/shimmer.dart';
import 'package:stayscanner/common/colors.dart';
import 'package:stayscanner/common/constants.dart';
import 'package:stayscanner/dto/Airbnb.dart';
import 'package:stayscanner/util/marker_maker.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:io';

import 'dialogs.dart';
import 'enlarge.dart';

class Stay extends StatelessWidget{
  String name;
  double rate;
  int reviews;
  String id;
  String price;
  String bookingType;
  String url;
  String thumb;
  bool isPlus;
  bool isSuperHost;
  bool isInstantBook;
  String accommodationType;

  String currencyCode;
  bool isSelected;

  BitmapDescriptor customIconSelected;
  BitmapDescriptor customIcon;

  bool clicked = false;
  NumberFormat currencySimbol = new NumberFormat();


  Stay(this.name, this.rate, this.reviews, this.id, this.price, this.bookingType,
      this.url, this.thumb, this.isPlus, this.isSuperHost, this.isInstantBook, this.accommodationType, this.currencyCode);



  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width * 0.7;
    double height = MediaQuery.of(context).size.height * 0.24;
    double aspectRatio = MediaQuery.of(context).devicePixelRatio;

    double bottom = aspectRatio > 2 ? MediaQuery.of(context).size.height * 0.015 : MediaQuery.of(context).size.height * 0.01;

    return Container(
      key: Key(id),
      padding: EdgeInsets.only(left: 12, right: 12, top: 0, bottom: 3),
      child: Stack(
        children: <Widget>[
          buildStayImage(width, height),
          buildStayOverlay(width, height, context),

          Container(
            color: Colors.transparent,
            width: width,
            height: 35,
            padding: EdgeInsets.only(top: 13, right: 3, left: 3),
            child:
            bookingType == "airbnb" ?
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                isPlus ? Container(
                    decoration: new BoxDecoration(
                        color: AppColors.blue,
                        border: Border.all(color: AppColors.blue, width: 2),
                        borderRadius: new BorderRadius.all(Radius.circular(5))),
                    child: Text(
                        'Plus',
                        style: TextStyle( color: Colors.white, fontSize: 14)
                    )
                ) : Container(),
                SizedBox(width: 10,),
                isSuperHost ? Container(
                    decoration: new BoxDecoration(
                        color: AppColors.blue,
                        border: Border.all(color: AppColors.blue, width: 2),
                        borderRadius: new BorderRadius.all(Radius.circular(5))),
                    child: Text(
                        'Superhost',
                        style: TextStyle( color: Colors.white, fontSize: 14)
                    )
                ) : Container(),
                SizedBox(width: 10,),
                isInstantBook? Container(
                    decoration: new BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: Colors.white, width: 2),
                        borderRadius: new BorderRadius.all(Radius.circular(5))),
                    child: Center(
                      child: Text(
                          'Instant Booking',
                          style: TextStyle( color: AppColors.blue, fontSize: 14, fontWeight: FontWeight.normal)
                      ),
                    )
                ) : Container()

              ],
            ) : Container(),
          ),

          Positioned(
              bottom: height/3.0,
              width: width,
              child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  child: Column(
                    children: <Widget>[
                      AutoSizeText(
                          name,
                          maxLines: 1,
                          textAlign: TextAlign.center,
                          style:TextStyle(
                              backgroundColor: Colors.transparent,
                              color: Colors.white,
                              fontSize: 14,
                              fontWeight: FontWeight.bold)),
                      AutoSizeText(accommodationType, maxLines: 1,maxFontSize: 14,
                          style: Theme.of(context).textTheme.display1),

                      Row(
                        mainAxisAlignment: (bookingType == "airbnb" && rate > 0.0) || (bookingType == "booking" && rate > 0.0) ?
                        MainAxisAlignment.spaceAround : MainAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              if(bookingType == "airbnb" && rate > 0.0 )
                                Icon(Icons.favorite, color: AppColors.blue,
                                  size: 16,),
                              if(bookingType == "airbnb" && rate > 0.0 )
                                Text(rate.toString() + " ", style: Theme
                                    .of(context)
                                    .textTheme
                                    .display2),
                              if(bookingType == "airbnb" && rate > 0.0 )
                                Text("(" + reviews.toString() + ")",
                                    style: Theme
                                        .of(context)
                                        .textTheme
                                        .display2),

                              if(bookingType == "booking" && rate>0)
                                StarRating(
                                    rating: rate,
                                    size: 14,
                                    starCount: rate.toInt(),
                                    color:  Theme
                                        .of(context).primaryColor
                                ),
                            ],
                          ),
                          Text(price+currencySimbol.simpleCurrencySymbol(currencyCode)+" total",style:Theme.of(context).textTheme.display2),

                        ],
                      ),

                    ],
                  )
              )

          ),

          Positioned.fill(child:
          Padding(
              padding: EdgeInsets.only(bottom: bottom),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: FlatButton(
                  color:  clicked ? Colors.grey : bookingType == "airbnb" ? Constants.airbnbColor : Constants.bookingColor,
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(Constants.radius),
//                      side: BorderSide(color: Colors.red)
                  ),
                  child: Text("Open"),
                  onPressed: () async {
                    print("STAY "+url.replaceAll("airbnb.com", "airbnb.co.uk"));
                    clicked = true;

                    if (Platform.isIOS && bookingType == "airbnb") {
                      if ( await launch(getAirbnbUrl(url), forceSafariVC: false, universalLinksOnly:false)) {
                      } else {
                        print("Fallback");
                        launch(url.replaceAll("airbnb.com", "airbnb.co.uk"), forceSafariVC: false);
                      }
                    }
                    else {
                      launch(url.replaceAll("airbnb.com", "airbnb.co.uk"), forceSafariVC: false);

                    }
                    (context as Element).markNeedsBuild();
                  },
                ),
              )
          )
          )

        ],
      ),
    );
  }



  GestureDetector buildStayOverlay(double width, double height, BuildContext context) {
    return
      GestureDetector(
        onTap: () {
          showGeneralDialog(barrierColor: Colors.black.withOpacity(0.5),
              transitionBuilder: (context, a1, a2, widget) {
                return Transform.scale(
                  scale: a1.value,
                  child: Opacity(
                    opacity: a1.value,
                    child: Center(
                      child: CachedNetworkImage(
                        fit: BoxFit.fitWidth,
                        height: 400,
                        imageUrl: thumb,
                        placeholder: (context, url) => CircularProgressIndicator(),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                    ),
                  ),
                );
              },
              transitionDuration: Duration(milliseconds: 300),
              barrierDismissible: true,
              barrierLabel: '',
              context: context,
              pageBuilder: (context, animation1, animation2) {});
        },
        child:Container(
          width: width,
          height: height,
          decoration: BoxDecoration(
              borderRadius: new BorderRadius.circular(7.0),
              color: Colors.white,
              gradient: LinearGradient(
                  begin: FractionalOffset.topCenter,
                  end: FractionalOffset.bottomCenter,
                  colors: [
                    Colors.white.withOpacity(0.0),
                    Colors.black.withOpacity(1.0),
                  ],
                  stops: [
                    0.4,
                    1.0
                  ])),
        ) ,
      );
  }

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  Container buildStayImage(double width, double height) {
    return
      Container(
          width: width,
          height: height,
          decoration: isSelected ? myBoxDecoration() : null,

          child: Padding(
            padding: EdgeInsets.only(top: 4),
            child: ClipRRect(
              borderRadius: new BorderRadius.circular(7.0),
              child: CachedNetworkImage(
                fit: BoxFit.fill,
                imageUrl: thumb,
                placeholder: (context, url) =>
                    Opacity(
                        opacity: 0.8,
                        child: Container(
                          width: width,
                          height: height,
                          color: Colors.white,
                          child: Image.asset('img/loading.gif',scale: 2,),
                        )
                    ),
                errorWidget: (context, url, error) =>
                    Opacity(
                      opacity: 0.8,
                      child: Container (
                        width: width,
                        height: height,
                        color: AppColors.blue,
                        child: Image.asset("img/StayScannerLogo.png"),
                      ),
                    ),
              ),
            ),
          )
      );
  }

  BoxDecoration myBoxDecoration() {
    return BoxDecoration(

      border: Border(
        top: BorderSide( //                   <--- left side
          color: AppColors.blue,
          width: 5.0,
        ),
      ),
    );
  }

  void generateIcons() async {
    MarkerMaker markerMaker = new MarkerMaker(null, null, id, price, bookingType, null, currencyCode);
    markerMaker.getMarkerIconSelected(bookingType,  Size(300.0, 300.0)).then((icon) => customIconSelected = icon);
    markerMaker.getMarkerIcon(bookingType,  Size(200.0, 200.0)).then((icon) => customIcon = icon);

  }

  RegExp intRegex = RegExp('www.airbnb.com\/rooms\/[0-9]*', multiLine: true);

  String getAirbnbUrl(String url) {
    String root = intRegex.allMatches(url).map((m) => m.group(0)).toString().replaceAll("(", "").replaceAll(")", "");

    String second = url.split(root).last.replaceAll("p.", "");

    root = root.replaceAll("www.airbnb.com", "airbnb:/");

    return root +"?"+second;
  }
}
