
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:stayscanner/common/colors.dart';

class Dialogs {
  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key, bool internet) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  key: key,
                  backgroundColor: Colors.white,
                  children: <Widget>[
                    internet ?
                    Align(
                      alignment: Alignment.center,
                      child: Text("No Internet Conection! ", style: Theme.of(context).textTheme.display1.copyWith(color: AppColors.blue))
                    ) : Container(),
                    internet ? Container(
                      padding: EdgeInsets.only(bottom: 10),
                      child: Icon(
                        Icons.signal_wifi_off,
                        color: AppColors.blue,
                        size: 60.0,
                      ),
                    ) : Container(),
                    internet ? Container() : Container(
                      child: Image.asset('img/loading.gif',scale: 2,),
                    ),
                  ]));
        });
  }


}
