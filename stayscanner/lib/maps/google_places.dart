library flutter_google_places.src;

import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:http/http.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PlacesAutocompleteWidget extends StatefulWidget {
  final String apiKey;
  final String hint;
  final Location location;
  final num offset;
  final num radius;
  final String language;
  final String sessionToken;
  final List<String> types;
  final List<Component> components;
  final bool strictbounds;
  final Mode mode;
  final Widget logo;
  final FocusNode focusNode;
  final ValueChanged<PlacesAutocompleteResponse> onError;
  ValueChanged<Prediction> onPressed;
  final bool validate;
  final IconData icon;


  /// optional - sets 'proxy' value in google_maps_webservice
  ///
  /// In case of using a proxy the baseUrl can be set.
  /// The apiKey is not required in case the proxy sets it.
  /// (Not storing the apiKey in the app is good practice)
  final String proxyBaseUrl;

  /// optional - set 'client' value in google_maps_webservice
  ///
  /// In case of using a proxy url that requires authentication
  /// or custom configuration
  final BaseClient httpClient;

  PlacesAutocompleteWidget(
      {@required this.apiKey,
        this.mode = Mode.fullscreen,
        this.hint = "Search",
        this.offset,
        this.location,
        this.radius,
        this.language,
        this.sessionToken,
        this.types,
        this.components,
        this.strictbounds,
        this.logo,
        this.onError,
        this.onPressed,
        this.focusNode,
        Key key,
        this.proxyBaseUrl,
        this.httpClient,
        this.icon,
        this.validate})
      : super(key: key);

  @override
  State<PlacesAutocompleteWidget> createState() {
    if (mode == Mode.fullscreen) {
      return _PlacesAutocompleteScaffoldState();
    }
    return _PlacesAutocompleteOverlayState();
  }

  static PlacesAutocompleteState of(BuildContext context) =>
      context.ancestorStateOfType(const TypeMatcher<PlacesAutocompleteState>());
}

class _PlacesAutocompleteScaffoldState extends PlacesAutocompleteState {
  @override
  Widget build(BuildContext context) {
    final appBar = AppBar(title: AppBarPlacesAutoCompleteTextField());
    final body = PlacesAutocompleteResult(
      onTap: Navigator.of(context).pop,
      logo: widget.logo,
    );
    return Scaffold(appBar: appBar, body: body);
  }
}

class _PlacesAutocompleteOverlayState extends PlacesAutocompleteState {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    widget.focusNode.addListener(() {
      if (!widget.focusNode.hasFocus) {
        if (this.mounted){
          setState(() {
            _doneSearching = true;
//            _searching = false;
          });
        }
        if (widget.focusNode.hasFocus) {
          setState(() {
            _doneSearching = false;
            _searching = true;
          });
        }
      }

    });

  }


  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final header = Column(children: <Widget>[
      Material(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: _textField(context),
              ),
            ],
          )),

    ]);

    var body;


    if (_searching) {
//      setState(() {
//        _doneSearching = false;
//      });
      body = Stack(
        children: <Widget>[Container()],
        alignment: FractionalOffset.bottomCenter,
      );
    } else if (_queryTextController.text.isEmpty ||
        _response == null ||
        _response.predictions.isEmpty) {
      body = Container();
    } else {
      body = SingleChildScrollView(
        child: Material(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(2.0),
            bottomRight: Radius.circular(2.0),
          ),
          color: theme.dialogBackgroundColor,
          child: ListBody(
            children: _response.predictions
                .map(
                  (p) => PredictionTile(
                  prediction: p,
                  onTap: (p) {
                    _queryTextController.text = p.description;
                    widget.onPressed(p);
                    widget.focusNode.unfocus();
                  }
              ),
            )
                .toList(),
          ),
        ),
      );
    }

    final container = Container(
      //margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 30.0),
        child: Stack(children: <Widget>[
          header,
          Padding(padding: EdgeInsets.only(top: 48.0), child: body),
        ]));

    if (Platform.isIOS) {
      return Padding(padding: EdgeInsets.only(top: 8.0), child: container);
    }
    return container;
  }

  Icon get _iconBack =>
      Platform.isIOS ? Icon(Icons.arrow_back_ios) : Icon(Icons.arrow_back);

  Widget _textField(BuildContext context) => Material
    (elevation: 3,
      shadowColor: Colors.grey,
      child: TextFormField(
          controller: _queryTextController,
          focusNode: widget.focusNode,
          cursorColor: Colors.black,
          keyboardAppearance: Brightness.dark,
          keyboardType: TextInputType.text,
          validator: widget.validate ? (value) {
            if (value.isEmpty) {
              return 'Enter the location value';
            }
            return null;
          } : null,
          decoration: InputDecoration(
            border: InputBorder.none,

            hintText: widget.hint != null ? widget.hint : "try \"London\"",
            prefixIcon: Padding(
              padding: EdgeInsets.all(0.0),
              child: Icon(
                widget.icon != null ? widget.icon : Icons.location_on,
                color: Theme.of(context).primaryColor,
              ), // icon is 48px widget.
            ),
            counterText: "",
            suffixIcon: _queryTextController.text.length > 0 ? IconButton(
              icon:Icon(
                Icons.clear,
                size: 20.0,
                color: Theme.of(context).primaryColor,
              ),
              onPressed: () => {
              SchedulerBinding.instance.addPostFrameCallback((_) {
                widget.focusNode.unfocus();
                _queryTextController.clear();
              })
              } ,
            ) : null,
            focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                    color: Colors.transparent
                )
            ),
          ),
          style: Theme.of(context).textTheme.title.copyWith(
              color: Colors.black,
              fontSize: 22
          )
      )
  );
}

class _Loader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        constraints: BoxConstraints(maxHeight: 2.0),
        child: LinearProgressIndicator(backgroundColor: Colors.transparent,));
  }
}

class PlacesAutocompleteResult extends StatefulWidget {
  final ValueChanged<Prediction> onTap;
  final Widget logo;

  PlacesAutocompleteResult({this.onTap, this.logo});

  @override
  _PlacesAutocompleteResult createState() => _PlacesAutocompleteResult();
}

class _PlacesAutocompleteResult extends State<PlacesAutocompleteResult> {
  @override
  Widget build(BuildContext context) {
    final state = PlacesAutocompleteWidget.of(context);
    assert(state != null);

    if (state._queryTextController.text.isEmpty ||
        state._response == null ||
        state._response.predictions.isEmpty) {
      final children = <Widget>[];
      if (state._searching) {
        children.add(_Loader());
      }
      return Stack(children: children);
    }
    return PredictionsListView(
      predictions: state._response.predictions,
      onTap: widget.onTap,
    );
  }
}

class AppBarPlacesAutoCompleteTextField extends StatefulWidget {
  @override
  _AppBarPlacesAutoCompleteTextFieldState createState() =>
      _AppBarPlacesAutoCompleteTextFieldState();
}

class _AppBarPlacesAutoCompleteTextFieldState
    extends State<AppBarPlacesAutoCompleteTextField> {
  @override
  Widget build(BuildContext context) {
    final state = PlacesAutocompleteWidget.of(context);
    assert(state != null);

    return Container(
        alignment: Alignment.topLeft,
        margin: EdgeInsets.only(top: 4.0),
        child: TextField(
          controller: state._queryTextController,
          autofocus: true,
          style: TextStyle(
            fontFamily: "Arboria",
            color: Theme.of(context).brightness == Brightness.light
                ? Colors.black.withOpacity(0.9)
                : Colors.white.withOpacity(0.9),
            fontSize: 16.0,
          ),
          decoration: InputDecoration(
            hintText: state.widget.hint,
            filled: true,
            fillColor: Theme.of(context).brightness == Brightness.light
                ? Colors.white30
                : Colors.black38,
            hintStyle: TextStyle(
              color: Theme.of(context).brightness == Brightness.light
                  ? Colors.black38
                  : Colors.white30,
              fontSize: 16.0,
              fontFamily:  "Arboria",
            ),
            border: InputBorder.none,
          ),
        ));
  }
}


class PredictionsListView extends StatelessWidget {
  final List<Prediction> predictions;
  final ValueChanged<Prediction> onTap;

  PredictionsListView({@required this.predictions, this.onTap});

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: predictions
          .map((Prediction p) => PredictionTile(prediction: p, onTap: onTap))
          .toList(),
    );
  }
}

class PredictionTile extends StatelessWidget {
  final Prediction prediction;
  final ValueChanged<Prediction> onTap;

  PredictionTile({@required this.prediction, this.onTap});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(Icons.location_on),
      title: Text(prediction.description),
      onTap: () {
        if (onTap != null) {
          onTap(prediction);
        }
      },
    );
  }
}

enum Mode { overlay, fullscreen }

abstract class PlacesAutocompleteState extends State<PlacesAutocompleteWidget> {
  TextEditingController _queryTextController;
  PlacesAutocompleteResponse _response;
  GoogleMapsPlaces _places;
  bool _searching;

  bool _doneSearching;

  final _queryBehavior = BehaviorSubject<String>.seeded('');

  @override
  void initState() {
    super.initState();
    _queryTextController = TextEditingController(text: "");

    _places = GoogleMapsPlaces(
        apiKey: widget.apiKey,
        baseUrl: widget.proxyBaseUrl,
        httpClient: widget.httpClient);
    _searching = false;
    _doneSearching = false;
    _queryTextController.addListener(_onQueryChange);

    _queryBehavior.stream
        .debounceTime(const Duration(milliseconds: 300))
        .listen(doSearch);
  }

  Future<Null> doSearch(String value) async {
    if (mounted && value.isNotEmpty) {
      setState(() {
        _searching = true;
      });

      final res = await _places.autocomplete(
        value,
        offset: widget.offset,
        location: widget.location,
        radius: widget.radius,
        language: widget.language,
        sessionToken: widget.sessionToken,
        types: widget.types,
        components: widget.components,
        strictbounds: widget.strictbounds,
      );

      if (res.errorMessage?.isNotEmpty == true ||
          res.status == "REQUEST_DENIED") {
        onResponseError(res);
      } else {
        onResponse(res);
      }
    } else {
      onResponse(null);
    }
  }

//  void fetchCity() async {
//    SharedPreferences prefs = await SharedPreferences.getInstance();
//    if(prefs.getBool('photographer') != null) {
//      var city = prefs.getString('city');
//      setState(() {
//        _queryTextController.text =city;
//        _searching = false;
//        _doneSearching = true;
//      });
//    }
//
//  }

  void _onQueryChange() {
    _queryBehavior.add(_queryTextController.text);
    setState(() {
      _doneSearching = false;
    });
  }

  @override
  void dispose() {
    super.dispose();

    _places.dispose();
    _queryBehavior.close();
    _queryTextController.removeListener(_onQueryChange);
  }

  @mustCallSuper
  void onResponseError(PlacesAutocompleteResponse res) {
    if (!mounted) return;

    if (widget.onError != null) {
      widget.onError(res);
    }
    setState(() {
      _response = null;
      _searching = false;
    });
  }

  @mustCallSuper
  void onResponse(PlacesAutocompleteResponse res) {
    if (!mounted) return;

    if(!_doneSearching) {
      setState(() {
        _response = res;
        _searching = false;
      });
    }
  }
}

class PlacesAutocomplete {
  static Future<Prediction> show({@required BuildContext context,
    @required String apiKey,
    Mode mode = Mode.fullscreen,
    String hint = "Search",
    num offset,
    Location location,
    num radius,
    String language,
    String sessionToken,
    List<String> types,
    List<Component> components,
    bool strictbounds,
    Widget logo,
    ValueChanged<PlacesAutocompleteResponse> onError,
    String proxyBaseUrl,
    Client httpClient}) {


    final builder = (BuildContext ctx) =>
        PlacesAutocompleteWidget(
            apiKey: apiKey,
            mode: mode,
            language: language,
            sessionToken: sessionToken,
            components: components,
            types: types,
            location: location,
            radius: radius,
            strictbounds: strictbounds,
            offset: offset,
            hint: hint,
            logo: logo,
            onError: onError,
            proxyBaseUrl: proxyBaseUrl,
            httpClient: httpClient);

    return showDialog(context: context, builder: builder);
  }
}